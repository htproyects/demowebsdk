<?php
/*
Plugin Name: Demo Sdk
*/

add_action('wp_enqueue_scripts', 'enqueue_scripts_and_styles');
function enqueue_scripts_and_styles() {
	wp_enqueue_script('script0', plugin_dir_url(__FILE__) . 'demowebsdk/js/hooptap-sdk-prod.js');
    wp_enqueue_script('script1', plugin_dir_url(__FILE__) . 'demowebsdk/js/main.js', array('jquery'));
    wp_enqueue_style( 'css0', plugin_dir_url(__FILE__) . 'demowebsdk/scss/main.css');

    wp_localize_script('script1', 'pluginScript', array(
	    'pluginsUrl' => plugin_dir_url(),
	));

}



?>